import React, { useCallback, useState } from 'react';

export default function Demo1WithState() {
  const [count, setCount] = useState<number>(0);
  const [random, setRandom] = useState<number>(0);

  const inc = useCallback(() => {
    setCount(c => c + 1);
  }, [])

  console.log('------\nApp: render')
  return (
    <div className="comp">
      <h3>Demo Hooks: useState</h3>
      <button onClick={() => setCount(count - 1)}>-</button>
      <button onClick={inc}>+</button>
      <button onClick={() => setRandom(Math.random())}>Random</button>
      <Dashboard count={count} random={random} increment={inc} />
    </div>
  );
}

interface DashboardProps {
  count: number;
  random: number;
  increment: () => void;
}

const Dashboard: React.FC<DashboardProps> = React.memo(props => {
  console.log(' Dashboard: render')
  return <div className="comp">
    Dashboard
    <CounterPanel value={props.count} />
    <RandomPanel value={props.random}/>
    <Buttons increment={props.increment} />
  </div>
})

function CounterPanel (props: { value: number }) {
  console.log('  CounterPanel: render')
  return <div className="comp">
    CounterPanel: {props.value}
  </div>
}

const RandomPanel: React.FC<{ value: number }> = React.memo((props) => {
  console.log('  Random Panel: render')
  return <div className="comp">
    RandomPanel: {props.value}
  </div>
})

const Buttons: React.FC<{ increment: () => void }> = React.memo(props => {
  console.log('  Buttons: render')
  return <div className="comp">
    <button onClick={props.increment}>update counter</button>
  </div>
})
