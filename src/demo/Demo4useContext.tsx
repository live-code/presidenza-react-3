import React, { useContext, useReducer } from 'react';

interface AppState {
  count: number;
  random: number;
}

type ActionType = 'increment' | 'decrement' | 'random';
type Action = { type: ActionType, payload?: AppState}

function appReducer(state: AppState, action: Action) {
  switch (action.type) {
    case 'decrement': return ({...state, count: state.count - 1});
    case 'increment': return ({...state, count: state.count + 1});
    case 'random': return ({...state, random: Math.random()});
    default: return state;
    // default: throw new Error(`Unhandled action: ${action.type}`)
  }
}

type Dispatch = (action: Action) => void;
const AppContext = React.createContext<AppState | undefined>( undefined)
const DispatchContext = React.createContext<Dispatch>(() => null);


const initialState = { count: 1, random: 0};

export default function Demo4useContext() {
  console.log('App: render')
  const [state, dispatch] = useReducer(appReducer, initialState);

  return (
    <AppContext.Provider value={state}>
      <DispatchContext.Provider value={dispatch}>
        <h1>Demo Hooks: Context & useReducer</h1>
        <Dashboard />
      </DispatchContext.Provider>
    </AppContext.Provider>
  );
}

const Dashboard: React.FC = React.memo(props => {
  console.log(' Dashboard: render')
  return <div className="comp">
    <Buttons />
    <CounterPanel />
    <RandomPanel  />
  </div>
})

function Buttons () {
  console.log('  Buttons: render')
  const dispatch = useContext(DispatchContext);

  return <div className="comp">
    <button onClick={() => dispatch({ type: 'decrement'})}>-</button>
    <button onClick={() => dispatch({ type: 'increment'})}>+</button>
    <button onClick={() => dispatch({ type: 'random'})}>random</button>
  </div>;
}

function CounterPanel () {
  console.log('  CounterPanel: render')
  const state = useContext(AppContext)

  return <div className="comp">Count: {state?.count}</div>
}

const RandomPanel: React.FC = React.memo(() => {
  console.log('  Random Panel: render')
  const state = useContext(AppContext)
  return <div className="comp">Random Value: {state?.random}</div>
})
