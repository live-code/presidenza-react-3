import React, { useReducer } from 'react';

interface AppState {
  count: number;
  random: number;
}

type AppActionsType = 'decrement' | 'increment' | 'random';
interface AppActions {
  type: AppActionsType;
  payload?: any;
}

function reducer(state: AppState, action: AppActions) {
  switch (action.type) {
    case 'decrement': return ({ ...state, count: state.count - 1})
    case 'increment': return ({ ...state, count: state.count + action.payload})
    case 'random': return ({ ...state, random: Math.random()})
  }
  return state;
}

export default function Demo3useReducer() {
  console.log('------\nApp: render')
  const [state, dispatch] = useReducer(reducer, { count: 1, random: 0})

  return (
    <div className="comp">
      <h3>Demo Hooks: useReducer</h3>
      <button onClick={() => dispatch({ type: 'decrement'})}>-</button>
      <button onClick={() => dispatch({ type: 'increment', payload: 10})}>+</button>
      <button onClick={() => dispatch({ type: 'random'})}>random</button>
      <Dashboard count={state.count} random={state.random} />
    </div>
  );
}

const Dashboard: React.FC<AppState> = props => {
  console.log(' Dashboard: render')
  return <div className="comp">
    Dashboard
    <CounterPanel value={props.count} />
    <RandomPanel value={props.random} />
  </div>
}


const CounterPanel: React.FC<{ value: number }> = ((props) => {
  console.log('  CounterPanel: render')
  return <div className="comp">
    Count: {props.value}
  </div>
})

const RandomPanel: React.FC<{ value: number }> = React.memo((props) => {
  console.log('  Random Panel: render')
  return <div className="comp">
    Random Value: {props.value}
  </div>
})
