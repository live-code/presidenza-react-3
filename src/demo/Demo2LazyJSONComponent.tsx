import React, { FunctionComponent, Suspense, useState } from 'react';

const WidgetChart = React.lazy(() => import('./components/WidgetChart'));
const WidgetProfile = React.lazy(() => import('./components/WidgetProfile'));

const JSON: any = [
  {
    component: 'profile',
    data: { id: 1, title: 'yo!', color: 'red' }
  },
  {
    component: 'chart',
    data: { id: 2, title: 'sono un chart!' }
  },
/*  {
    component: 'tabbar',
    data: { id: 3, items: [10, 20, 30], active: 2 }
  },*/
];

interface ComponentType {
  [key: string]: FunctionComponent<any>
}
const COMPONENTS: ComponentType = {
  chart: WidgetChart,
  profile: WidgetProfile,
}

export default function Demo2LazyJSONComponent() {

  return (
    <Suspense fallback={<div>loading</div>}>
      {
        JSON.map((item: any, index: number) => {
          const component: any = React.createElement(COMPONENTS[item.component], { data: item.data, key: index})
          return component
        })
      }

    </Suspense>
  );
}

