import React, { useState } from 'react';
import { ChipList } from './components/ChipList';
import { ChipSelector } from './components/ChipSelector'; // To use confirmPopup method
import { Accordion, AccordionTab } from 'primereact/accordion';


export default function App() {
  const [chips, setChips] = useState<string[]>(['pippo', 'pluto', 'topolino'])
  const [active, setActive] = useState<number>(0)

  return (
    <div>
      <ChipSelector
        chips={chips}
        onChipChange={chips => setChips(chips)}
      />

      <ChipList
        chips={chips}
        onChipDelete={chipToDelete => setChips(chips.filter(chip => chip !== chipToDelete))} />

      <Accordion activeIndex={active} onTabChange={(e) => setActive(e.index)}>
        <AccordionTab header="Header I">
          Content I
        </AccordionTab>
        <AccordionTab header="Header II">
          Content II
        </AccordionTab>
        <AccordionTab header="Header III">
          Content III
        </AccordionTab>
        {
          chips.map(chip => {
            return  <AccordionTab header={chip}>
              Content {chip}
            </AccordionTab>
          })
        }
      </Accordion>

    </div>
  );
}
