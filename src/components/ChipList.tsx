import React  from 'react';
import { confirmPopup } from 'primereact/confirmpopup';
import { Tooltip } from 'primereact/tooltip';

interface ChipListProps {
  chips: string[];
  onChipDelete: (chip: string) => void;
}
export const ChipList: React.VFC<ChipListProps> = (props) => {

  const confirm = (e: React.MouseEvent, chipToDelete: string) => {
    console.log(e)
    confirmPopup({
      target: e.currentTarget,
      message: 'Are you sure you want to proceed?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        props.onChipDelete(chipToDelete);
        // setChips(chips.filter(chip => chip !== chipToDelete));
      },
      reject: () => console.log('reject')
    });
  }

  return <div>
    {
      props.chips.map((chip, index) => {
        return <li key={index}>
          {chip}

          <Tooltip target=".pi-trash" content="Cancella" />
          <i className="pi pi-trash" onClick={(e) => confirm(e, chip)} />

        </li>
      })
    }
  </div>
};
