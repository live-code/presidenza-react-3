import React  from 'react';
import { Chips } from 'primereact/chips';

interface ChipSelectorProps {
  chips: string[];
  onChipChange: (value: string[]) => void;
}
export const ChipSelector: React.VFC<ChipSelectorProps> = (props) => {
  function customChip(item: string) {
    return (
      <div>
        <span>{item} </span>
        <i className="pi pi-user-plus" />
      </div>
    );
  }
  return <>
    <h1>Chip List</h1>
    <Chips value={props.chips} onChange={(e) => props.onChipChange(e.value)}  itemTemplate={customChip}/>
    <hr/>
    </>
};
